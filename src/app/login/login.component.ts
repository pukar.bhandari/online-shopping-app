import {Component, OnInit} from '@angular/core';
import {Router} from "@angular/router";
import {FormBuilder, FormGroup, Validators} from "@angular/forms";
import {ProductService} from "../shop/product.service";

@Component({
    selector: 'app-login',
    templateUrl: './login.component.html',
    styleUrls: ['./login.component.css']
})

export class LoginComponent implements OnInit {
    title= 'Nepali Amazon';
    loginForm: FormGroup;
    invalid: boolean = false;

    constructor(private fb: FormBuilder, private router: Router, private productService: ProductService) {
    }

    ngOnInit(): void {

        this.loginForm = this.fb.group({
            username: ['', Validators.required],
            password: ['', Validators.required]
        });
    }

    onAuthorize(): void {
        localStorage.removeItem('user_token');
        localStorage.removeItem('username');
        this.productService.getToken(this.loginForm.value)
            .subscribe(
                user => {
                    localStorage.setItem('user_token', user.token);
                    localStorage.setItem('username',user.user_display_name);
                    this.onAuthComplete();
                },
                err => {
                    this.invalid = true;
                    this.loginForm.reset();
                });

    }

    onAuthComplete(): void {
        this.router.navigate(['/shop']);
    }

}
