import { Injectable } from '@angular/core';
import {CanActivate,Router} from '@angular/router';

@Injectable({
  providedIn: 'root'
})
export class LoginGuard implements CanActivate {
  constructor(private router: Router) { }

  canActivate() {
    const loginToken = localStorage.getItem('user_token');
    if (loginToken) {
      // logged in so return true
      // this.router.navigate(['/shop']);
      return true;
    }

    // not logged in so redirect to login page with the return url
    this.router.navigate(['/login']);
    return false;
  }
}
