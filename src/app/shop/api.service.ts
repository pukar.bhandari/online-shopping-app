import {Injectable} from '@angular/core';
import {HttpClient, HttpHeaders} from "@angular/common/http";
import {Observable} from "rxjs";
import {ConfigService} from "../shared/config.service";

@Injectable({
    providedIn: 'root'
})

export class ApiService {
    private token = localStorage.getItem('admin_token');

    constructor(private http: HttpClient) {}

    get(url: string): Observable<any> {
        return this.http.get(`${ConfigService.API_URL}${url}`);
    }

    postToken(data): Observable<any> {
        const headers = new HttpHeaders({
            'Content-Type': 'application/json',
            'accept': 'application/json',
        });
        return this.http.post(`${ConfigService.TOKEN_URL}`, JSON.stringify(data), {headers})
    }

    post(url: string, data): Observable<any> {
        const headers = new HttpHeaders({
            'Content-Type': 'application/json',
            'accept': 'application/json'
        });
        return this.http.post(`${ConfigService.API_URL}${url}`, JSON.stringify(data), {headers})
    }

    put(url: string, data): Observable<any> {
        const headers = new HttpHeaders({'Content-Type': 'application/json', 'Authorization': 'Bearer ' + this.token});
        return this.http.put(`${ConfigService.API_URL}${url}`, JSON.stringify(data), {headers})
    }

    delete(url: string): Observable<any> {
        const headers = new HttpHeaders({'Content-Type': 'application/json','Authorization': 'Bearer ' + this.token});
        return this.http.delete(`${ConfigService.API_URL}${url}`, {headers})
    }

    postData(url: string, data: FormData): Observable<any> {
        return this.http.post(`${ConfigService.API_URL}${url}`, data);
    }
}
