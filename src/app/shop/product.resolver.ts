import {Injectable} from '@angular/core';
import {ActivatedRouteSnapshot, Resolve} from '@angular/router';
import {ProductService} from "./product.service";

@Injectable()
export class ProductsResolver implements Resolve<any> {

    constructor(private productService: ProductService) {
    }

    resolve() {
        return this.productService.getProducts();
    }
}

@Injectable()

export class ProductResolver implements Resolve<any> {

    constructor(private productService: ProductService) {
    }

    resolve(route: ActivatedRouteSnapshot) {
        let id = +route.params['id'];
        return this.productService.getBlog(id);
    }
}
