export interface Product {
    id: number,
    title: string,
    content: string,
    acf: {price: number},
    image: any,
    date: Date
}