import {Injectable} from '@angular/core';
import {Observable, of} from "rxjs";
import {ApiService} from "./api.service";
import {tap} from "rxjs/operators";
import {Product} from "./product";
import {Cart} from "./cart";

@Injectable({
    providedIn: 'root'
})
export class ProductService {
    private posts: Product[];

    constructor(private API: ApiService) {
    }

    getProducts(): Observable<any> {
        if (this.posts) {
            return of(this.posts);
        }
        return this.API.get('posts?_embed')
            .pipe(
                tap(data => this.posts = data)
            );
    }

    getBlog(id: number): Observable<Product> {
        if (this.posts) {
            const foundItem = this.posts.find(item => item.id === id);
            if (foundItem) {
                return of(foundItem);
            }
        }
        return this.API.get('id');
    }

    getToken(post): Observable<any> {
        return this.API.postToken(post);
    }

    addUser(user): Observable<any> {
        return this.API.post('users', user);
    }

    addProduct(product: FormData): Observable<any> {
        return this.API.postData('posts?_embed', product);
    }

    addMedia(media: FormData): Observable<any> {
        return this.API.postData('media', media);
    }

    updateProduct(product, id: number): Observable<any> {
        return this.API.put(`posts/${id}?_embed`, product)
            .pipe(
                tap(data => {
                    this.posts.push(data);
                })
            )
    }

    addToCart(order: FormData): Observable<any> {
        return this.API.postData('order', order);
    }
}
