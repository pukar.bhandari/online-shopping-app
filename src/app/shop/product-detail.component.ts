import {Component, OnInit} from '@angular/core';
import {Product} from "./product";
import {ActivatedRoute, Router} from "@angular/router";
import {Cart} from "./cart";
import {ProductService} from "./product.service";
import {DataService} from "../shared/data.service";
import {FormBuilder, FormGroup, Validators} from "@angular/forms";

@Component({
    selector: 'app-product-detail',
    templateUrl: './product-detail.component.html',
    styleUrls: ['./product-detail.component.css']
})
export class ProductDetailComponent implements OnInit {
    post: Product;
    price: number;
    cart: Cart[] = [];
    itemsInCart: number = null;
    quantity: FormGroup;

    constructor(private route: ActivatedRoute, private router: Router, private productService: ProductService,private dataService: DataService,private fb: FormBuilder) {
    }

    ngOnInit() {
        this.post = this.route.snapshot.data.post;
        this.quantity = this.fb.group({
           quantity: [1,Validators.min(1)]
        });
        this.dataService.data.subscribe(
            items => this.itemsInCart = items
        );
        if (localStorage.getItem("items_in_cart")) {
            this.cart = JSON.parse(localStorage.getItem("items_in_cart"));
            this.itemsInCart = this.cart.length;
        }
    }

    addToCart(id: number): void {
        this.price = +this.post.acf['price'];
        let data = {
            id: this.post.id,
            title: this.post.title,
            price: this.price,
            quantity: +this.quantity.get('quantity').value
        };
        this.cart.push(data);
        localStorage.setItem("items_in_cart", JSON.stringify(this.cart));
        this.dataService.updatedDataSelection(this.itemsInCart + 1)
    }

    onBack(): void {
        this.router.navigate(['/shop']);
    }
}
