import {Component, OnInit} from '@angular/core';
import {ProductService} from "./product.service";
import {ActivatedRoute, NavigationEnd, Router} from "@angular/router";
import {DataService} from "../shared/data.service";

@Component({
    selector: 'app-shopping',
    templateUrl: './shopping.component.html',
    styleUrls: ['./shopping.component.css']
})
export class ShoppingComponent implements OnInit {
    posts = [];
    title = 'Nepali Amazon';
    isLoggedIn: boolean;
    userName: string;
    showList: boolean;
    itemsInCart: number = null;

    constructor(private productService: ProductService, private route: ActivatedRoute, private router: Router,private dataService: DataService) {
    }

    ngOnInit() {
        this.dataService.data.subscribe(
            items => this.itemsInCart = items
        );

        this.getProducts();
        this.showList = this.route.snapshot.children.length === 0;
        if (localStorage.getItem('user_token')) {
            this.isLoggedIn = true;
            this.userName = localStorage.getItem('username');
        }
        this.router.events.subscribe((event) =>{
            if (event instanceof NavigationEnd) {
                this.showList = event.url === '/shop';
                if (this.showList){
                    this.getProducts();
                }
            }
        })
    }

    private getProducts(): void{
        this.posts = this.route.snapshot.data.posts;
    }

    onLogOut(): void {
        if (confirm("Are you sure you want to logout?")) {
            localStorage.removeItem('user_token');
            localStorage.removeItem('username');
            this.router.navigate(['/login']);
        }
    }
}