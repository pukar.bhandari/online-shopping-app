import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { Product.EditComponent } from './product.edit.component';

describe('Product.EditComponent', () => {
  let component: Product.EditComponent;
  let fixture: ComponentFixture<Product.EditComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ Product.EditComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(Product.EditComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
