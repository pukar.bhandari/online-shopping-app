import { Injectable } from '@angular/core';
import {CanActivate, CanDeactivate, Router} from '@angular/router';
import {CheckoutComponent} from "./checkout.component";

@Injectable({
    providedIn: 'root'
})
export class CheckoutGuard implements CanActivate,CanDeactivate<CheckoutComponent> {
    constructor(private router: Router) { }

    canActivate() {
        const itemsInCart = JSON.parse(localStorage.getItem('items_in_cart'));
        if (itemsInCart.length) {
            return true;
        }
        this.router.navigate(['/shop/cart']);
        return false;
    }

    canDeactivate(component: CheckoutComponent) {
        if (component.customerForm.dirty) {
            return confirm('Navigate away and lose all changes to your order?');
        }
        return true;
    }
}