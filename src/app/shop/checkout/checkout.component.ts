import { Component, OnInit } from '@angular/core';
import {Cart} from "../cart";
import {FormBuilder, FormGroup, Validators} from "@angular/forms";
import {ProductService} from "../product.service";
import {Router} from "@angular/router";
import {DataService} from "../../shared/data.service";

@Component({
  selector: 'app-checkout',
  templateUrl: './checkout.component.html',
  styleUrls: ['./checkout.component.css']
})
export class CheckoutComponent implements OnInit {
  cartItems: Cart[] = [];
  totalAmount: number;
  customerForm: FormGroup;
  orderPosted: boolean = false;

  constructor(private fb: FormBuilder,private productService: ProductService,private router: Router,private dataService: DataService) { }

  ngOnInit() {
    this.customerForm = this.fb.group({
      name: ['',Validators.required],
      phoneNumber:['',Validators.required],
      email: ['',Validators.required],
      deliveryAdd: ['',Validators.required],
      status: 'publish'
    });

    if (localStorage.getItem("items_in_cart")) {
      this.cartItems = JSON.parse(localStorage.getItem("items_in_cart"));
      this.totalAmount = +localStorage.getItem("total_amount");
    }
  }

  onCheckOut(): void{
    const formData = new FormData();
    formData.append('title',`Order for ${this.customerForm.get('name').value}`);
    formData.append('content',JSON.stringify(this.cartItems));
    formData.append('status', this.customerForm.get('status').value);
    formData.append('fields[total_amount]', this.totalAmount.toString());
    formData.append('fields[customer_name]',this.customerForm.get('name').value );
    formData.append('fields[phone_number]',this.customerForm.get('phoneNumber').value );
    formData.append('fields[email]',this.customerForm.get('email').value );
    formData.append('fields[delivery_address]',this.customerForm.get('deliveryAdd').value );
    this.productService.addToCart(formData).subscribe(post => {
      this.customerForm.reset();
      this.cartItems.splice(0,this.cartItems.length);
      localStorage.setItem('items_in_cart',JSON.stringify(this.cartItems));
      this.dataService.updatedDataSelection(0);
      this.orderPosted = true;
    });
  }
}
