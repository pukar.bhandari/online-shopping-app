import {Component, OnInit} from '@angular/core';
import {Cart} from "./cart";
import {ProductService} from "./product.service";
import {ActivatedRoute} from "@angular/router";
import {DataService} from "../shared/data.service";
import {FormBuilder, FormGroup, Validators} from "@angular/forms";

@Component({
    selector: 'app-cart',
    templateUrl: './cart.component.html',
    styleUrls: ['./cart.component.css']
})

export class CartComponent implements OnInit {
    cartItems: Cart[] = [];
    price: number[] = [];
    totalAmount: number;
    itemsInCart: number;
    quantity: FormGroup;

    constructor(private productService: ProductService, private route: ActivatedRoute, private dataService: DataService, private fb: FormBuilder) {
    }

    ngOnInit() {
        this.dataService.data.subscribe(
            items => this.itemsInCart = items
        );
        this.sum();
        this.quantity = this.fb.group({
            quantity: [null, Validators.min(1)]
        });
    }

    sum(): void {
        if (localStorage.getItem("items_in_cart")) {
            this.cartItems = JSON.parse(localStorage.getItem("items_in_cart"));
            if (this.cartItems.length) {
                for (let i = 0; i < this.cartItems.length; i++) {
                    this.price.push(this.cartItems[i].price * this.cartItems[i].quantity);
                }
                this.totalAmount = this.price.reduce(this.addFunc);
                localStorage.setItem('total_amount', this.totalAmount.toString());
            }
        }
    }

    onChange(quant: number, id: number): void {
        const foundIndex = this.cartItems.findIndex(item => item.id === id);
        if (foundIndex > -1) {
            this.cartItems[foundIndex].quantity = quant;
        }
        localStorage.setItem("items_in_cart", JSON.stringify(this.cartItems));
        this.price = [];
        this.sum();
    }

    addFunc(total, num) {
        return total + num;
    }

    onDelete(id: number) {
        const foundIndex = this.cartItems.findIndex(item => item.id === id);
        const currentPrice = this.cartItems.filter(item => item.id === id);
        this.totalAmount -= (currentPrice[0].price * currentPrice[0].quantity);
        localStorage.setItem('total_amount', this.totalAmount.toString());
        if (foundIndex > -1) {
            this.cartItems.splice(foundIndex, 1);
            this.dataService.updatedDataSelection(this.itemsInCart - 1);
        }
        localStorage.setItem("items_in_cart", JSON.stringify(this.cartItems));
    }
}
