import {Component, OnInit} from '@angular/core';
import {FormBuilder, FormGroup, Validators} from "@angular/forms";
import {ProductService} from "./product.service";
import {Router} from "@angular/router";
import {Observable} from "rxjs";
import {mergeMap} from "rxjs/operators";

@Component({
    selector: 'app-product-edit',
    templateUrl: './product.edit.component.html',
    styleUrls: ['./product.edit.component.css']
})

export class ProductEditComponent implements OnInit {
    productForm: FormGroup;
    fileToUpload: any;

    constructor(private fb: FormBuilder, private productService: ProductService, private router: Router) {
    }

    ngOnInit() {
        this.productForm = this.fb.group({
            title: ['', [Validators.required, Validators.minLength(5)]],
            content: ['', Validators.required],
            price: ['',[Validators.required, Validators.max(500000)]],
            image: ['', Validators.required],
            status: 'publish'
        })
    }

    onFileSelect(files: FileList) {
        if (files.length > 0) {
            this.fileToUpload = files.item(0);
            this.productForm.get('image').setValue(this.fileToUpload);
        }
    }

    addImage(post): Observable<any> {
        const mediaData = new FormData();
        mediaData.append('post', post.id);
        mediaData.append('file', this.productForm.get('image').value);
        return this.productService.addMedia(mediaData);
    }

    updatePro(post, media) {
        let data = {
            featured_media: media.id
        };
        return this.productService.updateProduct(data, post.id)
    }

    save() {
        const formData = new FormData();
        formData.append('title', this.productForm.get('title').value);
        formData.append('content', this.productForm.get('content').value);
        formData.append('status', this.productForm.get('status').value);
        formData.append('fields[price]', this.productForm.get('price').value);
        this.productService.addProduct(formData)
            .pipe(
                mergeMap(post => this.addImage(post)
                    .pipe(
                        mergeMap(data => this.updatePro(post, data))
                    )))
            .subscribe(post => this.router.navigate(['/shop'])

            );
    }
}
