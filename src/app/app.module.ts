import {BrowserModule} from '@angular/platform-browser';
import {NgModule} from '@angular/core';

import {AppComponent} from './app.component';
import {ShoppingComponent} from './shop/shopping.component';
import {HTTP_INTERCEPTORS, HttpClientModule} from "@angular/common/http";
import {LoginComponent} from './login/login.component';
import {DateAgoPipe} from './shared/date-ago.pipe';
import {RouterModule} from "@angular/router";
import {ReactiveFormsModule} from "@angular/forms";
import {ProductResolver, ProductsResolver} from "./shop/product.resolver";
import {BasicAuthInterceptor} from "./interceptor/http.interceptor";
import {RegisterComponent} from './register/register.component';
import {ProductEditComponent} from './shop/product.edit.component';
import { ProductDetailComponent } from './shop/product-detail.component';
import {ShopGuard} from "./shop/shop.guard";
import { CartComponent } from './shop/cart.component';
import {LoginGuard} from "./login/login.guard";
import { CheckoutComponent } from './shop/checkout/checkout.component';
import {CheckoutGuard} from "./shop/checkout/checkout.guard";

@NgModule({
    declarations: [
        AppComponent,
        ShoppingComponent,
        LoginComponent,
        DateAgoPipe,
        RegisterComponent,
        ProductEditComponent,
        ProductDetailComponent,
        CartComponent,
        CheckoutComponent
    ],
    imports: [
        BrowserModule,
        ReactiveFormsModule,
        HttpClientModule,
        RouterModule.forRoot([
            {
                path: 'login', component: LoginComponent,
                canActivate: [ShopGuard]
            },
            {path: 'register', component: RegisterComponent},
            {
                path: 'shop', component: ShoppingComponent,
                resolve: {
                    posts: ProductsResolver
                },
                children: [
                    {
                        path: 'add',
                        component: ProductEditComponent,
                        canActivate: [LoginGuard]
                    },
                    {
                        path: 'details/:id',
                        component: ProductDetailComponent,
                        resolve: {
                            post: ProductResolver
                        }
                    },
                    {
                        path: 'cart',
                        component: CartComponent
                    },
                    {
                        path: 'checkout',
                        component: CheckoutComponent,
                        canActivate: [CheckoutGuard],
                        canDeactivate: [CheckoutGuard]
                    }
                ]
            },
            // {path: 'add-product', component:  ProductEditComponent},
            {path: '', redirectTo: 'shop', pathMatch: 'full'},
            {path: '**', redirectTo: 'shop', pathMatch: 'full'}
        ])
    ],
    providers: [ProductsResolver,
        ProductResolver,
        {provide: HTTP_INTERCEPTORS, useClass: BasicAuthInterceptor, multi: true}],
    bootstrap: [AppComponent]
})
export class AppModule {
}
