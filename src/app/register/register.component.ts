import { Component, OnInit } from '@angular/core';
import {FormBuilder, FormGroup, Validators} from "@angular/forms";
import {ProductService} from "../shop/product.service";
import {Router} from "@angular/router";

@Component({
  selector: 'app-register',
  templateUrl: './register.component.html',
  styleUrls: ['./register.component.css']
})
export class RegisterComponent implements OnInit {
  title= 'Nepali Amazon';
  registerForm: FormGroup;
  invalid: boolean = false;

  constructor(private fb: FormBuilder,private productService: ProductService,private router: Router) { }

  ngOnInit() {
    this.registerForm = this.fb.group({
      username: ['',[Validators.required,Validators.minLength(5)]],
      email: ['',Validators.required],
      firstName: ['',[Validators.required,Validators.minLength(3)]],
      lastName: ['',[Validators.required,Validators.maxLength(50)]],
      password: ['',[Validators.required,Validators.minLength(3)]],
      roles: ['']
    })
  }
   onAuthorize(): void{
     this.productService.addUser(this.registerForm.value).subscribe(data => {
       console.log(data);
       this.router.navigate(['/login'])
     }, err =>this.invalid = true );
   }
}
