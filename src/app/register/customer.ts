export interface Customer {
    username: string,
    email: string,
    firstName: string,
    lastName: string,
    password: string,
    roles: string
}