import {Injectable} from '@angular/core';
import { HttpRequest, HttpHandler, HttpEvent, HttpInterceptor } from '@angular/common/http';
import { Observable } from 'rxjs';
import {ApiService} from "../shop/api.service";

@Injectable()
export class BasicAuthInterceptor implements HttpInterceptor {
    constructor(private API: ApiService) { }

    intercept(request: HttpRequest<any>, next: HttpHandler): Observable<HttpEvent<any>> {
        // add authorization header with basic auth credentials if available
        const {url,method} = request;
        let token;
        token = localStorage.getItem('admin_token'); // auth is provided via constructor.
        if (token && (url.endsWith('/users') || url.endsWith('/posts?_embed')|| url.endsWith('/media') || url.endsWith('/order'))
            && (method === 'POST'||method === 'PUT' || method === 'DELETE')) {
            // Logged in. Add Bearer token.
            request= request.clone({
                setHeaders: {
                    Authorization:
                        `Bearer ${token}`
                }
            });
        }
        return next.handle(request);
    }
}