import { Injectable } from '@angular/core';
import {environment} from "../../environments/environment";

@Injectable()

export class ConfigService {
  public static API_URL: string = environment.apiurl;
  public static TOKEN_URL: string = 'http://localhost/test/wordpress/wp-json/jwt-auth/v1/token';
}
