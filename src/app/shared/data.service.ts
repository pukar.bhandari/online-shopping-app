import { Injectable } from '@angular/core';
import {BehaviorSubject} from "rxjs";


@Injectable({
  providedIn: 'root'
})

export class DataService {
  private items: number = JSON.parse(localStorage.getItem('items_in_cart')).length;
  private itemsInCart = this.items? this.items : 0;
  private dataSource = new BehaviorSubject(this.itemsInCart);
  data = this.dataSource.asObservable();

  constructor() { }

  updatedDataSelection(data: number){
    this.dataSource.next(data);
  }
}